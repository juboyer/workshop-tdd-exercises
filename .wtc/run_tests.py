from airtable import Airtable


def update_student_data(student_name, exercise_shortcode, succeeds, fails, last_failed_msg):
    """
    Updates the record for student, adding or updating nr of commits, successes and fails for the indicated exercise
    * if no record found, then create new record for student
    * also update Totals
    :param student_name:
    :param exercise_shortcode:
    :param succeeds:
    :param fails:
    :return:
    """
    print('Updating {} result for {}'.format(exercise_shortcode, student_name))

    airtable = Airtable('appgztWMqt6aKZrxJ', 'workshop1', 'keyPe4F8xrLW8W9Ao')
    result = airtable.match('Student', student_name)
    print('* match result: '+str(result))

    if len(result) == 0:
        print("* creating new record for "+student_name)
        record = {
            'Student': student_name,
            'Total Commits': 1,
            'Total Succeeding': 1 if fails == 0 else 0,
            'Total Failing': 1 if fails > 0 else 0,
            '{} - Commits'.format(exercise_shortcode): 1,
            '{} - Succeeded'.format(exercise_shortcode): succeeds,
            '{} - Failed'.format(exercise_shortcode): fails,
            'Last Failed Test': last_failed_msg
        }
        airtable.insert(record)
    else:
        print('* updating existing record {} for {}'.format(result['id'],student_name))
        total_success = result['fields']['Total Succeeding']
        total_fails = result['fields']['Total Failing']
        now_succeeds = fails == 0
        if '{} - Succeeded'.format(exercise_shortcode) in result['fields']:
            #already there, and being updated
            prev_fails = result['fields']['{} - Failed'.format(exercise_shortcode)]
            prev_commits = result['fields']['{} - Commits'.format(exercise_shortcode)]
            if prev_fails == 0 and not now_succeeds:
                #previously succeeded and now not anymore
                total_success -= 1
                total_fails += 1
            elif prev_fails > 0 and now_succeeds:
                #previously failed and now succeeds
                total_success += 1
                total_fails -= 1

        else:
            #new exercise being added
            prev_commits = 0
            if fails == 0:
                total_success += 1
            else:
                total_fails += 1

        record = {
            'Total Commits': result['fields']['Total Commits'] + 1,
            'Total Succeeding': total_success,
            'Total Failing': total_fails,
            '{} - Commits'.format(exercise_shortcode): prev_commits + 1,
            '{} - Succeeded'.format(exercise_shortcode): succeeds,
            '{} - Failed'.format(exercise_shortcode): fails,
            'Last Failed Test': last_failed_msg
        }
        airtable.update(result['id'], record)


def get_student_email():
    """
    use git config to get student's email address
    :return:
    """
    import subprocess
    out = subprocess.check_output('git config user.email', shell=True)
    student_name = out.decode("utf-8").strip()
    if len(student_name) == 0:
        student_name = 'UNKNOWN'
    return student_name


def run_unittests(path):
    """
    Use this method to discover unittests at specified path, and run them
    :param path:
    :return: TestResult
    """
    import unittest
    loader = unittest.TestLoader()
    suite = loader.discover(start_dir=path)

    runner = unittest.TextTestRunner()
    return runner.run(suite)


def run_test_exercise(exercise_path, initial_expected_fails):
    print('* Running tests for '+exercise_path)
    result = run_unittests(exercise_path)
    print("---------- Tests Result: {} ----------\n Succeeded: {} | Failed: {} | Errors: {}\n-----------------------------------------------\n".format(exercise_path, result.wasSuccessful(), len(result.failures), len(result.errors)))
    exercise_short_code = exercise_path[:1].upper() + exercise_path[1:2] + exercise_path[-3:]

    if not len(result.failures) == initial_expected_fails:
        #some work done on exercise, so submit results
        num_succeeds = result.testsRun if result.wasSuccessful() else result.testsRun - len(result.failures) - len(result.errors)
        last_failed_msg = ''
        if len(result.failures) > 0:
            (testcase, error) = result.failures[0]
            last_failed_msg = str(testcase)
        elif len(result.errors) > 0:
            (testcase, error) = result.errors[0]
            last_failed_msg = str(testcase)

        print('*** Last Failed Message: '+last_failed_msg)

        update_student_data(student_name, exercise_short_code, num_succeeds, len(result.failures) + len(result.errors), last_failed_msg)
    else:
        print('* Not yet started on {} - not publishing results'.format(exercise_path))


def main():
    print('Starting Python run tests script')
    run_test_exercise('exercise001', 4)
    run_test_exercise('exercise002', 6)
    run_test_exercise('exercise003', 3)
    print('Finished Python run tests script')


student_name = ''

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        student_name = sys.argv[1]
    else:
        student_name = get_student_email()

    main()
